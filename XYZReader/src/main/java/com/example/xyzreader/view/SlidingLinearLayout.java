/*
*
 XYZ Reader

 Simple news reader app

 Copyright (C) 2016 Kairu Joshua Wambugu

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see http://www.gnu.org/licenses/.
* */

package com.example.xyzreader.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.LinearLayout;

/**
 * A LinearLayout that slides.
 * */
// begin class SlidingLinearLayout
public class SlidingLinearLayout extends LinearLayout {

    /* CONSTANTS */
    
    /* Integers */
    
    /* Strings */
        
    /* VARIABLES */

    /* On Pre Draw Listeners */

    // OnPreDrawListener - a callback to be invoked when the view tree is about to be drawn.
    private OnPreDrawListener preDrawListener = null; // ditto

    /* Primitives */

    private float yFraction = 0; // the fraction of the view's Y that occupies the screen
    
    /* CONSTRUCTOR */

    public SlidingLinearLayout( Context context ) {
        super( context );
    }

    public SlidingLinearLayout( Context context, AttributeSet attrs ) {
        super( context, attrs );
    }

    public SlidingLinearLayout( Context context, AttributeSet attrs, int defStyleAttr ) {
        super( context, attrs, defStyleAttr );
    }

    /* METHODS */
    
    /* Getters and Setters */

    // begin setter for the yFraction
    public void setYFraction( final float yFraction ) {

        // 0. initialize the y fraction
        // 1. if the view's height is zero
        // 1a. if the pre draw listener is not yet initialized
        // 1a1. during pre draw
        // 1a1a. remove this pre draw listener since we will not be using it afterwards
        // 1a1b. set the y fraction
        // 1a1c. return true
        // 1b. use this pre draw listener
        // 1c. finish
        // 2. the actual translationY =
        // total height * fraction of the view's y that's currently on screen
        // 3. initialize the translationY with the value at 2

        // 0. initialize the y fraction

        this.yFraction = yFraction;

        // 1. if the view's height is zero

        // begin if view height is zero
        if ( getHeight() == 0 ) {

            // 1a. if the pre draw listener is not yet initialized

            // begin if the pre draw listener is null
            if ( preDrawListener == null ) {

                // 1a1. during pre draw

                // begin preDrawListener = new OnPreDrawListener
                preDrawListener = new OnPreDrawListener() {

                    @Override
                    // begin onPreDraw
                    public boolean onPreDraw() {

                        // 1a1a. remove this pre draw listener since we will not be using it afterwards

                        getViewTreeObserver().removeOnPreDrawListener( preDrawListener );

                        // 1a1b. set the y fraction

                        setYFraction( yFraction );

                        // 1a1c. return true

                        return true;

                    } // end onPreDraw

                }; // end preDrawListener = new OnPreDrawListener

            } // end if the pre draw listener is null

            // 1b. use this pre draw listener

            getViewTreeObserver().addOnPreDrawListener( preDrawListener );

            // 1c. finish

            return;

        } // end if view height is zero

        // 2. the actual translationY =
        // total height * fraction of the view's y that's currently on screen

        float translationY = getHeight() * yFraction;

        // 3. initialize the translationY with the value at 2

        setTranslationY( translationY );

    } // end setter for the yFraction

    // getter for the yFraction
    public float getYFraction() { return yFraction; }

    /* Overrides */
    
    /* Other Methods */
    
    /* INNER CLASSES */

} // end class SlidingLinearLayout