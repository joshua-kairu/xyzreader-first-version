/*
*
 XYZ Reader

 Simple news reader app

 Copyright (C) 2016 Kairu Joshua Wambugu

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see http://www.gnu.org/licenses/.
* */

package com.example.xyzreader.ui;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.graphics.Palette;
import android.text.Html;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.xyzreader.R;
import com.example.xyzreader.data.ArticleLoader;
import com.example.xyzreader.view.SlidingLinearLayout;

/**
 * A fragment representing a single Article detail screen. This fragment is
 * either contained in a {@link ArticleListActivity} in two-pane mode (on
 * tablets) or a {@link ArticleDetailActivity} on handsets.
 */
public class ArticleDetailFragment extends Fragment
        implements LoaderManager.LoaderCallbacks< Cursor > {

    /* CONSTANTS */

    /* Floats */

    private static final float PARALLAX_FACTOR = 1.25f;

    /* Integers */

    /* Strings */

    private static final String TAG = "ArticleDetailFragment";

    public static final String ARG_ITEM_ID = "item_id";

    /* VARIABLES */

    /* Collapsing Toolbar Layouts */

    private CollapsingToolbarLayout mCollapsing; // the collapsing toolbar

    /* Cursors */

    private Cursor mCursor;

    /* Primitives */

    private boolean mIsCard = false;

    private long mItemId;

    private int mMutedColor = 0xFF333333;
    private int mStatusBarFullOpacityBottom;

    /* Image Views */

    private ImageView mPhotoView;

    /* Sliding Linear Layouts */

    private SlidingLinearLayout mTextHolderLinearLayout; // ditto

    /* Views */

    private View mRootView;

    /* CONSTRUCTOR */

    /* METHODS */

    /* Getters and Setters */

    /* Overrides */

    /* Other Methods */

    /* INNER CLASSES */
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ArticleDetailFragment() {
    }

    public static ArticleDetailFragment newInstance(long itemId) {

        Bundle arguments = new Bundle();

        arguments.putLong(ARG_ITEM_ID, itemId);

        ArticleDetailFragment fragment = new ArticleDetailFragment();

        fragment.setArguments(arguments);

        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mItemId = getArguments().getLong(ARG_ITEM_ID);
        }

        mIsCard = getResources().getBoolean(R.bool.detail_is_card);

        mStatusBarFullOpacityBottom = getResources().getDimensionPixelSize(
                R.dimen.detail_card_top_margin);

        setHasOptionsMenu(true);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        // In support library r8, calling initLoader for a fragment in a FragmentPagerAdapter in
        // the fragment's onCreate may cause the same LoaderManager to be dealt to multiple
        // fragments because their mIndex is -1 (haven't been added to the activity yet). Thus,
        // we do this in onActivityCreated.
        getLoaderManager().initLoader(0, null, this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        // 0. use the detail fragment layout
        // 1. put the title in the layout's collapsing toolbar

        // 0. use the detail fragment layout

        mRootView = inflater.inflate(R.layout.fragment_article_detail, container, false);

        // 1. put the title in the layout's collapsing toolbar -> done in bindViews

        mCollapsing = ( CollapsingToolbarLayout ) mRootView.findViewById( R.id.fad_ctl_collapser );

        mPhotoView = (ImageView) mRootView.findViewById(R.id.photo);

        mRootView.findViewById(R.id.share_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(Intent.createChooser(ShareCompat.IntentBuilder.from(getActivity())
                        .setType("text/plain")
                        .setText("Some sample text")
                        .getIntent(), getString(R.string.action_share)));

                Snackbar.make(
                        mRootView, R.string.snackbar_share_message, Snackbar.LENGTH_SHORT ).show();
            }
        });

        bindViews();
        return mRootView;
    }

    private void bindViews() {
        if (mRootView == null) {
            return;
        }

        TextView bylineView = (TextView) mRootView.findViewById(R.id.article_byline);
        bylineView.setMovementMethod(new LinkMovementMethod());
        TextView bodyView = (TextView) mRootView.findViewById(R.id.article_body);

        if (mCursor != null) {
            mRootView.setAlpha(0);
            mRootView.setVisibility(View.VISIBLE);
            mRootView.animate().alpha(1);

            mCollapsing.setTitle( mCursor.getString( ArticleLoader.Query.TITLE ) );
            mCollapsing.setCollapsedTitleTypeface(
                    Typeface.createFromAsset( getResources().getAssets(), "Rosario-Regular.ttf" ) );
            mCollapsing.setExpandedTitleTypeface(
                    Typeface.createFromAsset(getResources().getAssets(), "Rosario-Regular.ttf" ) );

            bylineView.setText(Html.fromHtml(
                    DateUtils.getRelativeTimeSpanString(
                            mCursor.getLong(ArticleLoader.Query.PUBLISHED_DATE),
                            System.currentTimeMillis(), DateUtils.HOUR_IN_MILLIS,
                            DateUtils.FORMAT_ABBREV_ALL).toString()
                            + " by "
                            + mCursor.getString(ArticleLoader.Query.AUTHOR)
                            ));
            bodyView.setText(Html.fromHtml(mCursor.getString(ArticleLoader.Query.BODY)));

            ImageLoaderHelper.getInstance(getActivity()).getImageLoader()
                    .get(mCursor.getString(ArticleLoader.Query.PHOTO_URL), new ImageLoader.ImageListener() {
                        @Override
                        public void onResponse(ImageLoader.ImageContainer imageContainer, boolean b) {
                            Bitmap bitmap = imageContainer.getBitmap();
                            if (bitmap != null) {
                                Palette p = Palette.generate(bitmap, 12);
                                mMutedColor = p.getDarkMutedColor(0xFF333333);
                                mPhotoView.setImageBitmap(imageContainer.getBitmap());
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                        }
                    });
            // animate the linear layout having the text

            mTextHolderLinearLayout =
                    ( SlidingLinearLayout ) mRootView.findViewById( R.id.fad_ll_text_holder );

            AnimatorSet fadeInSlideUpObjectAnimator =
                    ( AnimatorSet ) AnimatorInflater.loadAnimator(
                            getActivity(), R.animator.fade_in_and_slide_up );

            // setTarget - Set the target object
            //  whose property will be animated by this animation.
            //  If the animator has been started, it will be canceled.
            fadeInSlideUpObjectAnimator.setTarget( mTextHolderLinearLayout );

            fadeInSlideUpObjectAnimator.setInterpolator( new FastOutSlowInInterpolator() );

            fadeInSlideUpObjectAnimator.start();

        } else {
            mRootView.setVisibility(View.GONE);
            mCollapsing.setTitle("N/A");
            bylineView.setText("N/A" );
            bodyView.setText("N/A");
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return ArticleLoader.newInstanceForItemId(getActivity(), mItemId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (!isAdded()) {
            if (cursor != null) {
                cursor.close();
            }
            return;
        }

        mCursor = cursor;
        if (mCursor != null && !mCursor.moveToFirst()) {
            Log.e(TAG, "Error reading item detail cursor");
            mCursor.close();
            mCursor = null;
        }

        bindViews();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mCursor = null;
        bindViews();
    }

}
