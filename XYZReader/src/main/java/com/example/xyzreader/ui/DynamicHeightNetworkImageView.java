package com.example.xyzreader.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;

public class DynamicHeightNetworkImageView extends NetworkImageView {
    private float mAspectRatio = 1.5f; // making this a 3:2 aspect ratio

    public DynamicHeightNetworkImageView(Context context) {
        super(context);
    }

    public DynamicHeightNetworkImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicHeightNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setAspectRatio(float aspectRatio) {
        mAspectRatio = aspectRatio;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        // 0. get the size of the width
        // 1. height should be 2 / 3 of the width for a 3:2 aspect ratio
        // 2. pass width and height to super

        // 0. get the size of the width

        int width = MeasureSpec.getSize( widthMeasureSpec );

        // 1. height should be 2 / 3 of the width for a 3:2 aspect ratio

        int desiredHeight = width * 2 / 3;

        // 2. pass width and height to super

        super.onMeasure( widthMeasureSpec,
                MeasureSpec.makeMeasureSpec( desiredHeight, MeasureSpec.EXACTLY ) );

//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        int measuredWidth = getMeasuredWidth();
//        setMeasuredDimension(measuredWidth, (int) (measuredWidth / mAspectRatio));
    }
}
